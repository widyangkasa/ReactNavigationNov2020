import React, {Component} from 'react';
import {View, Text} from 'react-native';
import DrawerNavigation from './Navigators/DrawerNavigation';

export default function App() {
  return (
    <DrawerNavigation />
  )
}
