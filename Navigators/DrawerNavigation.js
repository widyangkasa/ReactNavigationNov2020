import React from 'react';
import { View, StyleSheet, Text, Image} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList, DrawerItem} from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/FontAwesome5';

import Home from '../Screens/Home';
import Contact from '../Screens/Contact';
function DrawerContent(props) {
    return (
        <View style={styles.container}>
            <View style={styles.drawerHeader}>
                <Image source={require('../assets/img/pp.jpg')} style={styles.drawerProfilePic}/>
                <View  style={styles.drawerProfileInfo}>
                    <Text style={styles.drawerHeaderName}>John Smith</Text>
                    <Text style={styles.drawerHeaderUserName}>@johnsmith</Text>
                </View>
            </View>
            <View style={styles.drawerContent}>
                <DrawerContentScrollView {...props} >
                    <DrawerItemList {...props}></DrawerItemList>
                </DrawerContentScrollView>
                <DrawerItem style={styles.bottomDrawerItem}
                        icon={({color, size}) => (
                            <Icon name="sign-out-alt" color={color} size={size} />
                        )}
                        label="Sign out"
                        onPress={() => props.navigation.closeDrawer()}
                    />
            </View>
        </View>
    )
}

const Drawer = createDrawerNavigator();

function MyDrawer() {
    return (
        <Drawer.Navigator backBehaviour="history" drawerContent = {(props) =><DrawerContent {...props}/>}>
            <Drawer.Screen name="Home" component={Home} options={{
                drawerIcon: ({color,size})=>(
                    <Icon name="home" color={color} size={size} />
                )
            }}/>
            <Drawer.Screen name="Contact" component={Contact} options={{
                drawerIcon: ({color,size})=>(
                    <Icon name="whatsapp" color={color} size={size} />
                )
            }}/>
        </Drawer.Navigator>
    )
}

const styles = StyleSheet.create({
    container: {
        flex:1
    },
    drawerHeader: {
        height: 100,
        alignItems: 'center',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        paddingHorizontal: 20
    },
    drawerContent: {
        flexGrow: 1,
        justifyContent: 'flex-end'
    },
    drawerProfilePic:{
        width: 50,
        height: 50,
        borderRadius: 50/2
    },
    drawerProfileInfo:{
        flexDirection: 'column',
        marginLeft: 25
    },
    drawerHeaderName: {
        color: '#000',
        fontSize: 20,
        fontWeight: 'bold'
    },
    drawerHeaderUserName: {
        color: '#6d6d6d',
        fontSize: 15,
    },
    bottomDrawerItem: {
        borderTopWidth: 1,
        borderTopColor: '#f5f5f5',
        margin: 0
    }

})

export default function DrawerNavigation() {
    return (
        <NavigationContainer>
            <MyDrawer />
        </NavigationContainer>

    )
}